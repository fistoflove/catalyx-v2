<?php
/**
 * Child Starter functions and definitions
 *
 */
use Timber\Timber;
add_action('wp_head', function() {
	?>

	<?php
});

add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style('theme-override', get_stylesheet_directory_uri() . '/static/base.min.css', array(), '1.0.0', 'all');
});

add_action('wp_head', function() {
	Timber::render( 'core/favicons.twig');
});

add_action('wp_head', function() {
	$user = wp_get_current_user();
	if($user->exists()) {
		if(str_contains($user->user_email, '@imsmarketing.ie')) {
			?>
			<script>
			  window.markerConfig = {
				project: '64f71f95dff008ff0b119e0c', 
				source: 'snippet',
				reporter: {
					email: 'vini@imsmarketing.ie',
					fullName: 'IMS Marketing',
				},
			  };
			</script>
			<?php
		} else {
			?>
			<script>
			  window.markerConfig = {
				project: '64f71f95dff008ff0b119e0c', 
				source: 'snippet',
				reporter: {
					email: '<?= $user->user_email; ?>',
					fullName: '<?= $user->display_name; ?>',
				},
			  };
			</script>
			<?php
		}
		?>
		<script>
			!function(e,r,a){if(!e.__Marker){e.__Marker={};var t=[],n={__cs:t};["show","hide","isVisible","capture","cancelCapture","unload","reload","isExtensionInstalled","setReporter","setCustomData","on","off"].forEach(function(e){n[e]=function(){var r=Array.prototype.slice.call(arguments);r.unshift(e),t.push(r)}}),e.Marker=n;var s=r.createElement("script");s.async=1,s.src="https://edge.marker.io/latest/shim.js";var i=r.getElementsByTagName("script")[0];i.parentNode.insertBefore(s,i)}}(window,document);
		</script>
		<?php
	}
  });

  register_post_type( 'case-study', [
	'label'                 => __( 'Case Study', 'ims_theme' ),
	'description'           => __( 'Case Studies', 'ims_theme' ),
	'labels'                => [
		'name'                  => _x( 'Case Studies', 'Post Type General Name', 'ims_theme' ),
		'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'ims_theme' ),
		'menu_name'             => __( 'Case Studies', 'ims_theme' ),
		'name_admin_bar'        => __( 'Case Study', 'ims_theme' ),
		'archives'              => __( 'Case Study Archives', 'ims_theme' ),
		'attributes'            => __( 'Case Study Attributes', 'ims_theme' ),
		'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
		'all_items'             => __( 'All Case Studies', 'ims_theme' ),
		'add_new_item'          => __( 'Add Case Study', 'ims_theme' ),
		'add_new'               => __( 'Add Case Study', 'ims_theme' ),
		'new_item'              => __( 'New Case Study', 'ims_theme' ),
		'edit_item'             => __( 'Edit Case Study', 'ims_theme' ),
		'update_item'           => __( 'Update Case Study', 'ims_theme' ),
		'view_item'             => __( 'View Case Study', 'ims_theme' ),
		'view_items'            => __( 'View Case Studies', 'ims_theme' ),
		'search_items'          => __( 'Search Case Study', 'ims_theme' ),
		'not_found'             => __( 'Not found', 'ims_theme' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
		'featured_image'        => __( 'Featured Image', 'ims_theme' ),
		'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
		'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
		'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
		'insert_into_item'      => __( 'Insert into Case Study', 'ims_theme' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Case Study', 'ims_theme' ),
		'items_list'            => __( 'Case Studies list', 'ims_theme' ),
		'items_list_navigation' => __( 'Case Studies list navigation', 'ims_theme' ),
		'filter_items_list'     => __( 'Filter Case Studies list', 'ims_theme' ),
	],
	'supports'              => [ 'title', 'editor', 'custom-fields', 'thumbnail', 'excerpt' ],
	'hierarchical'          => true,
	'public'                => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'menu_position'         => 25,
	'menu_icon'             => 'dashicons-embed-post',
	'show_in_admin_bar'     => true,
	'show_in_nav_menus'     => false,
	'can_export'            => false,
	'has_archive'           => false,
	'exclude_from_search'   => false,
	'publicly_queryable'    => true,
	'rewrite'               =>	[
		'slug'                  => 'case-studies',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	],
	'capability_type'       => 'post',
	'show_in_rest'          => true,
]);

// function my_cs_query( $query ) {
// 	if ( $query->is_main_query()) {
// 	  $query->set( 'post_type', array( 'case-study', 'post'));
// 	}
//   }
// add_action( 'pre_get_posts', 'my_cs_query' );