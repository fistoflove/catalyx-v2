<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Headline", "text"],
        ["Icon01", "image"],
        ["Quote01", "text"],
        ["Icon02", "image"],
        ["Quote02", "text"],
        ["Icon03", "image"],
        ["Quote03", "text"],
        ["Icon04", "image"],
        ["Quote04", "text"],
        ["Icon05", "image"],
        ["Quote05", "text"],
        ["Icon06", "image"],
        ["Quote06", "text"],
        ["Link", "link"]
    ]
);

