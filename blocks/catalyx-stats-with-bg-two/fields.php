<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Shape", "image"],
        ["Headline", "text"],
        ["BoxPhoto01", "image"],
        ["BoxText01", "text"],
        ["BoxPhoto02", "image"],
        ["BoxText02", "text"],
        ["BoxPhoto03", "image"],
        ["BoxText03", "text"],
        ["BoxPhoto04", "image"],
        ["BoxText04", "text"],
        ["BoxPhoto05", "image"],
        ["BoxText05", "text"],
        ["BoxPhoto06", "image"],
        ["BoxText06", "text"]
    ]
);
