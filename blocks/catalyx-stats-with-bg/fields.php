<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Shape", "image"],
        ["Headline", "text"],
        ["UnderHeadline", "text"],
        ["icon01", "image"],
        ["TextBox01", "text"],
        ["icon02", "image"],
        ["TextBox02", "text"],
        ["icon03", "image"],
        ["TextBox03", "text"]
    ]
);
