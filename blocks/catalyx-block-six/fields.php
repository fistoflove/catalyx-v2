<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Arrow", "image"],
        ["Boxlink01", "link"],
        ["Boxlink02", "link"],
        ["Boxlink03", "link"],
        ["Boxlink04", "link"],
        ["Boxlink05", "link"],
        ["Boxlink06", "link"],
        ["Boxlink07", "link"],
        ["Boxlink08", "link"],
        ["Boxes", "repeater", [
            ["Link", "link"]
        ]]
    ]
);

