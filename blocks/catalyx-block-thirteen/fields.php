<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Headline", "text"],
        ["ParagraphLeft", "wysiwyg"],
        ["ParagraphRight", "wysiwyg"]
    ]
);
