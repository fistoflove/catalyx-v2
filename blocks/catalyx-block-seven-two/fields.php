<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Shape_Big", "image"],
        ["Shape_Medium", "image"],
        ["Right_Image", "image"],
    ]
);

