<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Headline", "text"],
        ["Logo01", "image"],
        ["Logo02", "image"],
        ["Logo03", "image"],
        ["Logo04", "image"]
    ]
);
