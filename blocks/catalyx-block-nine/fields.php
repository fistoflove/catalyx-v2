<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Headline", "text"],
        ["TextBox01", "text"],
        ["ShapeBox01", "image"],
        ["TextBox02", "text"],
        ["ShapeBox02", "image"],
        ["TextBox03", "text"],
        ["ShapeBox03", "image"],
        ["TextBox04", "text"],
        ["ShapeBox04", "image"],
        ["TextBox05", "text"],
        ["ShapeBox05", "image"],
        ["TextBox06", "text"],
        ["ShapeBox06", "image"]
    ]
);
